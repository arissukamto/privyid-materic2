import html2canvas from "html2canvas"

const Screenshoot = () => {
    const snap = () =>{
        html2canvas(document.body).then(function(canvas){
            var a = document.createElement('a')
            a.href = canvas.toDataURL("...assets/image/jpg").replace("image/jpg", "image/octet-system")
            a.download = `download.jpg`
            a.click()
        })
    } 
    return(<button onClick={snap}>Download Screenshot</button>);

}

export default Screenshoot
