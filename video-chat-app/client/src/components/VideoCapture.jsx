export default function Recorder(){
    const parts = [];
    let mediaRecorder;
    // Start Record
    const StartRecording = () =>{
        navigator.mediaDevices.getUserMedia({audio: true, video: true}).then(stream => {
            document.getElementById("video").srcObject = stream
            console.log('Start Recording')
            mediaRecorder = new MediaRecorder(stream)
            

            mediaRecorder.start(1000)
            mediaRecorder.ondataavailable = function (e){
                parts.push(e.data)
            }
        })
    }

    const StopRecording = () =>{
        console.log('Stop Record')

        mediaRecorder.stop()
        const blob = new Blob(parts,{
            type: "video/webm"
        });
        const url = URL.createObjectURL(blob)
        const a = document.createElement("a")
        document.body.appendChild(a)
        a.style = "display: none"
        a.href = url
        a.download = "test.webm"
        a.click()
    }

    // output
    return(
        <div>
            <button onClick={StartRecording}>Start Screen Record</button>
            <button onClick={StopRecording}>Stop Screen Record</button>

        </div>
    );

}
