import * as React from "react";

import useScreenRecorder from "use-screen-recorder";

const Recorder = () => {
  const {
    blobUrl,
    pauseRecording,
    resetRecording,
    resumeRecording,
    startRecording,
    status,
    stopRecording,
  } = useScreenRecorder({audio: true});

  return (
    
    <div>
        <div className="col">
        
            <div className="row">
                <small>Status: {status}</small>
            </div>
            <div className="row center-block">
                <button onClick={startRecording}>Start Recording</button>
                <button onClick={stopRecording}>Stop</button>
                <button onClick={pauseRecording}>Pause</button>
                <button onClick={resumeRecording}>Resume</button>
                <button onClick={resetRecording}>Reset</button>
            </div>
            <div className="row">
                <a href={blobUrl} download> Click to download video record</a>
            </div>
       </div>
    </div>

  );
};

export default Recorder
